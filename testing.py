import cv2
import sys
import os

flower_cascade = cv2.CascadeClassifier('data/cascade-20.xml')

fn = sys.argv[1]
img = cv2.imread(sys.argv[1])
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

print(img.shape)

flowers = flower_cascade.detectMultiScale(gray, 1.5, 5)

print(len(flowers))

for (x, y, w, h) in flowers:
    cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 10)

print('done')

cv2.imwrite(f'annotated/{os.path.basename(fn)}-annotated.png', img)
