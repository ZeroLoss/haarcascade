import os

'''Inputs'''
# -info: file the positive data
# (created with opencv_annotation, and possibly modified by annotatetocropped.py in imagetools)
# Ideally but not essentially square images
INFO = 'all-positives-no-buds-square.txt'
# -bg: filename containing list of locations of images
BG = 'bg.txt'

'''Outputs'''
# -vec: location to create the .vec file
VEC = 'flowers.vec'
# -data: data folder for training output
DATA = 'data'

'''Configuration'''
# -w and -h: width and height for training on
DIMENSION = 50
# -show: whether to show samples (ESC to quit out)
SHOW = False
# -num: number of samples to generate (ignored at the moment)
NUM = 1000

# Extra config args for opencv_traincascade
NUMPOS = 600
NUMNEG = 1200
NUMSTAGES = 20
MINHITRATE = 0.995
MAXFALSEALARMRATE = 0.5
PRECALCVALBUFSIZE = 2048
PRECALCIDXBUFSIZE = 2048

if __name__ == '__main__':
    f = os.system(f'opencv_createsamples'
                  f' -vec {VEC}'
                  f' -info {INFO}'
                  # f' -bg {BG}'
                  f' -w {DIMENSION}'
                  f' -h {DIMENSION}'
                  # f' -num {NUM}'
                  f'{" -show" if SHOW else ""}')

    if f == 0:
        os.system(f'opencv_traincascade'
                  f' -data {DATA}'
                  f' -vec {VEC}'
                  f' -bg {BG}'
                  f' -w {DIMENSION}'
                  f' -h {DIMENSION}'
                  f' -precalcValBufSize {PRECALCVALBUFSIZE}'
                  f' -precalcIdxBufSize {PRECALCIDXBUFSIZE}'
                  f' -numPos {NUMPOS}'
                  f' -numNeg {NUMNEG}'
                  f' -numStages {NUMSTAGES}'
                  f' -minHitRate {MINHITRATE}'
                  f' -maxFalseAlarmRate {MAXFALSEALARMRATE}')
