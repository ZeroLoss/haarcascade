import cv2
import os
import pathlib

if __name__ == '__main__':
    flower_cascade = cv2.CascadeClassifier('data/cascade-25.xml')

    pathlib.Path('annotated-test').mkdir(parents=True, exist_ok=True)

    for fn in os.listdir('images-test'):
        print(fn)

        img = cv2.imread(os.path.join('images-test', fn))
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        flowers = flower_cascade.detectMultiScale(gray, 1.1, 5)

        for (x, y, w, h) in flowers:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 10)

        cv2.imwrite(os.path.join('annotated-test', fn), img)
