import cv2
import os
import pathlib

flower_cascade = cv2.CascadeClassifier('data/cascade-30.xml')


def test_image(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    flowers = flower_cascade.detectMultiScale(gray, 10, 5)
    return len(flowers)


if __name__ == '__main__':

    pathlib.Path('confusion-debug').mkdir(parents=True, exist_ok=True)

    dirs = [('negatives-square.txt.test-cropped', 0),
            ('positives-square.txt.test-cropped', 1),
            ('negatives-square.txt.train-cropped', 0),
            ('positives-square.txt.train-cropped', 1)
            ]

    counts = {dirname: [0, 0] for dirname, desired in dirs}

    for dirname, desired in dirs:
        for fn in os.listdir(dirname):
            print(fn)
            img = cv2.imread(os.path.join(dirname, fn))
            test_result = test_image(img)
            if test_result == desired or (desired == 1 and test_result > 1):
                counts[dirname][0] += 1
            if test_result > 1:
                print(test_result)
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                flowers = flower_cascade.detectMultiScale(gray, 10, 5)
                for (x, y, w, h) in flowers:
                    cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 5)
                cv2.imwrite(f'confusion-debug/{dirname}-{fn}', img)
            counts[dirname][1] += 1

    print(counts)
