import re

scale_pattern = re.compile('SCALE: (\S+) \| NEIGHBOURS: (\S+)')
pattern = re.compile('([^:]+):.+\((.+)\)')

if __name__ == '__main__':
    with open('results/log.txt') as f:
        print('cascade,scale,neighbours,negative-train,negative-test,positive-train,positive-test')
        cascade = 0
        scale = 0
        neighbours = 0
        values = {
            'negative-train': 0,
            'negative-test': 0,
            'positive-train': 0,
            'positive-test': 0
        }

        for line in f.readlines():
            if line.startswith('CASCADE'):
                cascade = int(line[-3:])
            elif line.startswith('SCALE'):
                match = re.match(scale_pattern, line)
                scale = float(match.group(1))
                neighbours = int(match.group(2))
            else:
                match = re.match(pattern, line)
                if match is not None:
                    values[match.group(1)] = float(match.group(2))
                    if match.group(1) == 'positive-test':
                        print(f'{cascade},{scale},{neighbours},{values["negative-train"]},{values["negative-test"]},{values["positive-train"]},{values["positive-test"]}')
