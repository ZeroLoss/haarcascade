import cv2
import os
import sys

CASCADES = ['10', '15', '20', '25', '30']
DIRS = {
    'positives-square.txt.test-cropped': (True, 'positive-test'),
    'negatives-square.txt.test-cropped': (False, 'negative-test'),
    'positives-square.txt.train-cropped': (True, 'positive-train'),
    'negatives-square.txt.train-cropped': (False, 'negative-train'),
}
SCALES = [1.1, 1.5, 2, 3]
NEIGHS = [3, 5, 7, 9]


def test_image(img, scale, neigh):
    flowers = flower_cascade.detectMultiScale(img, scale, neigh)
    return len(flowers) >= 1


if __name__ == '__main__':
    images = {dirname: [] for dirname in DIRS.keys()}
    totals = {dirname: len(os.listdir(dirname)) for dirname in DIRS.keys()}

    for dirname in DIRS:
        for fn in os.listdir(dirname):
            img = cv2.imread(os.path.join(dirname, fn))
            images[dirname].append(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))

    for cascade in CASCADES:
        flower_cascade = cv2.CascadeClassifier('data/cascade-{0}.xml'.format(cascade))
        print('CASCADE: {0}'.format(cascade))
        for scale in SCALES:
            for neigh in NEIGHS:
                print('SCALE: {0} | NEIGHBOURS: {1}'.format(scale, neigh))
                for dirname, (target, label) in DIRS.items():
                    count = 0
                    for img in images[dirname]:
                        if test_image(img, scale, neigh) == target:
                            count += 1
                        print(count, file=sys.stderr)
                    print('{0}: {1}/{2} ({3:.2f})'.format(label, count, totals[dirname], 100*count/totals[dirname]))
        print()
